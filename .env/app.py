from flask import Flask, request
import pickle
import numpy as np

model = pickle.load(open("skincare_routine.pkl", 'rb'))
app = Flask(__name__)

@app.route('/')
def index():
    return "Hello Word"

@app.route('/skin', methods = ["POST"])
def skin():
    normal = request.form.get('normal')
    dry = request.form.get('dry')
    oily = request.form.get('oily')
    combination = request.form.get('combination')
    acne = request.form.get('acne')
    sensitive = request.form.get('sensitive')
    fine_lines = request.form.get('fine_lines')
    wrinkles = request.form.get('wrinkles')
    redness = request.form.get('redness')
    dull = request.form.get('dull')
    pore = request.form.get('pore')
    pigmentation = request.form.get('pigmentation')
    blackheads = request.form.get('blackheads')
    whiteheads = request.form.get('whiteheads')
    blackheads = request.form.get('blackheads')
    blemishes = request.form.get('blemishes')
    dark_circles = request.form.get('darkcircles')
    eye_bags = request.form.get('eye_bags')
    dark_spots = request.form.get('dark_spots')

    input_query = np.array([[normal, dry, oily, combination,acne,sensitive,fine_lines,wrinkles,redness,dull,pore,pigmentation,blackheads,blemishes,dark_circles,eye_bags,dark_spots]])


if __name__ =='__main__':
    app.run()


# from flask import Flask, request, jsonify
# import joblib # type: ignore

# app = Flask(__name__)
# model = joblib.load('skincare_routine.pkl')

# @app.route('/recommendation', methods=['POST'])
# def get_recommendation():
#     selected_options = request.json
#     # Use the selected options to make predictions using your model
#     # Replace this with your model prediction logic
#     recommendation = model.predict(selected_options)
#     return jsonify({'recommendation': recommendation})

# if __name__ == '__main__':
#     app.run(debug=True)
