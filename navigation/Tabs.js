import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { View, Image, TouchableOpacity, StyleSheet, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

import QuizPage from "../screens/QuizPage";
import RoutinePage from "../screens/RoutinePage";
import HomePage from "../screens/Homepage";
import ScannerPage from "../screens/ScannerPage"


const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({ children, onPress }) => (
  <TouchableOpacity
    style={{
      top: -50,
      right:50,
      // justifyContent: "center",
      // alignItems: "center",
      ...styles.shadow,
    }}
    onPress={onPress}
  >
    <View
      style={{
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: "#808080",
      }}
    >
      {children}
    </View>
  </TouchableOpacity>
);

const Tabs = () => {
  const navigation = useNavigation();

  const handleAddButtonClick = () => {
    navigation.navigate("AddScreen");
  };
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          position: "absolute",
          bottom: -1,
          left: -2,
          right: -1,
          elevation: 0,
          backgroundColor: "#008080",
          borderRadius: 15,
          height: 85,
          ...styles.shadow,
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomePage}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                top: 10,
              }}
            >
              <Image
                source={require("../assets/home.png")}
                resizeMode="contain"
                style={{
                  width: 35,
                  height: 35,
                  // tintColor: focused ? "#8F00FF" : "#748c94",
                }}
              />
              <Text
                style={{ color: focused ? "#00008B" : "#ffffff", fontSize: 12 }}
              >
                HOME
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="routine"
        component={RoutinePage}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                top: 10,
                right:-40,
              }}
            >
              <Image
                source={require("../assets/routine.png")}
                resizeMode="contain"
                style={{

                  width: 35,
                  height: 35,
                  // tintColor: focused ? "#8F00FF" : "#748c94",
                }}
              />
              <Text
                style={{ color: focused ? "#00008B" : "#ffffff", fontSize: 12 }}
              >
                ROUTINE
              </Text>
            </View>
          ),
        }}
      />

      {/* <Tab.Screen
        name="Scanner"
        component={ScannerPage}
        options={{
          tabBarButton: (props) => <CustomTabBarButton {...props} />,
          tabBarIcon: ({ focused }) => (
            <Image
              source={require("../assets/face.png")}
              resizeMode="contain"
              style={{
                width: 35,
                height: 35,
                tintColor: "#FFFFFF",
              }}
            />
          ),
        }}
      /> */}

<Tab.Screen
    name="Scanner"
    component={ScannerPage}
    options={({ route }) => ({
        tabBarButton: (props) => (
            <TouchableOpacity {...props} onPress={() => {
                // Access the takePicture function from the route params
                const takePicture = route.params?.takePicture;
                if (takePicture) {
                    takePicture();
                } else {
                    console.error('Camera not ready');
                }
            }}>
                <CustomTabBarButton {...props} />
            </TouchableOpacity>
        ),
        tabBarIcon: ({ focused }) => (
            <Image
                source={require("../assets/face.png")}
                resizeMode="contain"
                style={{
                    width: 35,
                    height: 35,
                    tintColor: focused ? "#FFFFFF" : "#000000", // Example for focused state color change
                }}
            />
        ),
    })}
/>

      <Tab.Screen
        name="Quiz"
        component={QuizPage}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                top: 10,
              }}
            >
              <Image
                source={require("../assets/quiz.png")}
                resizeMode="contain"
                style={{
                  width: 35,
                  height: 35,
                  // tintColor: focused ? "#8F00FF" : "#748c94",
                }}
              />
              <Text
                style={{ color: focused ? "#00008B" : "#ffffff", fontSize: 12 }}
              >
                QUIZ
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#7F5DF0",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
});

export default Tabs;

