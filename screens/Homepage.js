import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

const Homepage = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../assets/logo.jpg')} 
        style={styles.logo}
      />
      <View style={styles.textContainer}>
        <View style={styles.textContent}>
          <Text style={styles.welcomeText}>Welcome!</Text>
          <Text style={styles.descriptionText}>Don't forget to care for your skin</Text>
          <Text style={{
        color: "#ffffff"}}>Start Skin Care Routine</Text>
          
        </View>
        <Image
          source={require('../assets/sun.gif')} 
          style={styles.smallImage}
        />
      </View>
    </View>
  );
};

export default Homepage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f2f2f2",
  },
  logo: {
    width: "100%",
    height: 230,
    // resizeMode: 'contain',
   top:-190,
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '90%',
    height:150,
    backgroundColor:"#008080" ,
    borderRadius:15,
    top:-90,
  
    
  },
  textContent: {
    flex: 1, 
    left:10,
  },
  welcomeText: {
    fontSize: 24,
    fontWeight: 'bold',
    color:"#ffffff"
  },
  descriptionText: {
    fontSize: 16,
    color:"#ffffff"
  },
  smallImage: {
    width: 100, 
    height: 100,   
    borderRadius:45,
    right:10,
  },
});
