import React, { useState, useEffect, useRef } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Button } from 'react-native';
import { Camera } from 'expo-camera';

const ScannerPage = () => {
    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [flashMode, setFlashMode] = useState(Camera.Constants.FlashMode.off);
    const [photo, setPhoto] = useState(null);
    const cameraRef = useRef(null);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleFlip = () => {
        setType(type === Camera.Constants.Type.back ? Camera.Constants.Type.front : Camera.Constants.Type.back);
    };

    const toggleFlash = () => {
        setFlashMode(
            flashMode === Camera.Constants.FlashMode.off
            ? Camera.Constants.FlashMode.on
            : Camera.Constants.FlashMode.off
        );
    };

    const takePicture = async () => {
        if (cameraRef.current) {
            const options = { quality: 0.5, base64: true, skipProcessing: true };
            const data = await cameraRef.current.takePictureAsync(options);
            setPhoto(data.uri);
        }
    };

    const retakePicture = () => {
        setPhoto(null); // Clear the current photo and allow the user to retake a new photo
    };

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>
            {photo ? (
                <View style={styles.previewContainer}>
                    <Image style={styles.preview} source={{ uri: photo }} />
                    <Button title="Retake" onPress={retakePicture} />
                </View>
            ) : (
                <Camera style={styles.camera} type={type} flashMode={flashMode} ref={cameraRef}>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.flipButton} onPress={handleFlip}>
                            <Image
                                style={styles.flipImage}
                                source={require('../assets/flip.png')}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.flashButton} onPress={toggleFlash}>
                            <Image
                                style={styles.flashImage}
                                source={flashMode === Camera.Constants.FlashMode.off ? require('../assets/flash.png') : require('../assets/flash_on.png')}
                            />
                        </TouchableOpacity>
                        
                    </View>
                </Camera>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    camera: {
        flex: 1,
    },
    previewContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '100%',
        width: '100%',
    },
    buttonContainer: {
        position: 'absolute',
        top: 30,
        right: 20,
        flexDirection: 'column',
    },
    flipButton: {
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    flipImage: {
        width: 35,
        height: 35,
    },
    flashButton: {
        marginTop: 15,
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    flashImage: {
        width: 35,
        height: 35,
    },
   
});

export default ScannerPage;
