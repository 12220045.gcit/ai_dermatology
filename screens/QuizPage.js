import React, { useState } from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native';

const QuizPage = ({ navigation }) => {
  const [selectedOptions, setSelectedOptions] = useState({});
  const [showResult, setShowResult] = useState(false);
  const [resultText, setResultText] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const questions = [
    {
      id: 1,
      question: 'Do you have normal skin?',
      options: ['Yes', 'No'],
    },
    {
      id: 2,
      question: 'Do you have dry skin?',
      options: ['Yes', 'No'],
    },
    {
      id: 3,
      question: 'Do you have oily skin?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 4,
      question: 'Do you have combination skin?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 5,
      question: 'Do you have acne-prone skin?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 6,
      question: 'Do you have sensitive skin?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 7,
      question: 'Do you have fine lines?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 8,
      question: 'Do you have wrinkles?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 9,
      question: 'Do you experience redness?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 10,
      question: 'Do you have dull skin?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 11,
      question: 'Do you have enlarged pores?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 12,
      question: 'Do you have pigmentation issues?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 13,
      question: 'Do you have blackheads?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 14,
      question: 'Do you have whiteheads?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 15,
      question: 'Do you experience blemishes?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 16,
      question: 'Do you have dark circles?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 17,
      question: 'Do you have under-eye bags?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    },
    {
      id: 18,
      question: 'Do you have dark spots?',
      options: ['Yes', 'No'],
      correctAnswer: 'Yes'
    }

  ];

  const handleOptionSelect = (questionId, optionIndex) => {
    setSelectedOptions({
      ...selectedOptions,
      [questionId]: optionIndex,
    });
  };

  const handleSubmission = async () => {
    setIsLoading(true);
    const answeredQuestions = Object.keys(selectedOptions).length;
    if (answeredQuestions !== questions.length) {
      alert('Please answer all the questions before submitting.');
      setIsLoading(false);
      return;
  }
    try {
      const response = await fetch('http://your-backend-endpoint/recommendation', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(selectedOptions),
      });
      const data = await response.json();
      setResultText(data.recommendation);
    } catch (error) {
      console.error('Error:', error);
      setResultText("Error occurred. Please try again.");
    }
    setIsLoading(false);
    setShowResult(true);
  };
  



  if (showResult) {
    return (
      <View style={styles.container}>
        <Text style={styles.resultText}>{resultText}</Text>
        <Button title="Restart Quiz" onPress={() => setShowResult(false)} />
      </View>
    );
  }

  return (
    <ScrollView contentContainerStyle={styles.scrollViewContainer}>
      <View style={styles.container}>
        <Text style = {styles.title}> Quiz</Text>
        {/* <Image source={require('../assets/logo.jpg')} style={styles.logo} /> */}
        <View style={styles.questionContainer}>
          {questions.map((question, index) => (
            <View key={question.id} style={[styles.questionBox, index === 0 && styles.firstQuestionBox,styles.questionBox, index === questions.length - 1 && styles.lastQuestionBox ]}>
              <Text style={styles.questionText}>{question.question}</Text>
              {question.options.map((option, optionIndex) => (
                <TouchableOpacity
                  key={optionIndex}
                  style={styles.optionBox}
                  onPress={() => handleOptionSelect(question.id, optionIndex)}
                >
                  <View style={[styles.radioButton, selectedOptions[question.id] === optionIndex && styles.radioButtonSelected]}>
                    {selectedOptions[question.id] === optionIndex && <View style={styles.radioButtonInner} />}
                  </View>
                  <Text style={styles.optionText}>{option}</Text>
                </TouchableOpacity>
              ))}
            </View>
            
          ))}
        <TouchableOpacity
          style={styles.submitButton}
          onPress={handleSubmission}
          // disabled={Object.keys(selectedOptions).length !== questions.length || isLoading}
        >
          <Text style={styles.submitButtonText}>{isLoading ? "Loading..." : "Submit"}</Text>
        </TouchableOpacity>
        </View>
        
  
      </View>
    </ScrollView>
  );
};

export default QuizPage;


const styles = StyleSheet.create({
  scrollViewContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E8F5E9',
    marginBottom: 100
  },
  // logo: {
  //   width: 100,
  //   height: 100,
  //   position: 'absolute',
  //   top: 30,
  //   left: 20,
  // },
  title: {
    marginRight: 30,
    marginTop: 60,
    fontSize: 20,
    fontFamily: 'Arial', // Replace 'Arial' with your desired font family
    color: '#333', // Text color
    fontWeight: 'bold', // Font weight
  },

  questionContainer: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 100
  },
  questionBox: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    padding: 20,
    marginBottom: 20,
  },
  firstQuestionBox: {
    marginTop: 25, // Add padding to the top of the first question
  },
  lastQuestionBox: {
    marginBottom: 20, // Add extra margin to the last question
  },
  
  questionText: {
    fontSize: 18,
    marginBottom: 10,
    color: '#008080',
  },
  optionBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  radioButton: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#008080',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  radioButtonSelected: {
    backgroundColor: '#008080',
  },
  radioButtonInner: {
    width: 12,
    height: 12,
    borderRadius: 6,
    backgroundColor: '#008080',
  },
  optionText: {
    fontSize: 16,
    color: '#333333',
  },
  resultText: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: '#008080',
  },
  submitButton: {
    width: 200,
    height: 40,
    alignItems: 'center',
    position: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#008080',
    backgroundColor: 'transparent',
    marginBottom: 100,
  
  },
  submitButtonText: {
    color: '#008080',
    fontSize: 18,
    fontWeight: 'bold',
  },
  
});


