import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";

const RoutinePage = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text> night and morning routine</Text>
      <Button title="Click here" onPress={() => alert("Button Clicked!")} />
    </View>
  );
};

export default RoutinePage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f2f2f2",
  },
});
